<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html class="all-font-size" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('websetting.webtitle') }}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">

    <link rel="stylesheet" href="{{ asset('AdminLTE-master/bower_components/bootstrap/dist/css/bootstrap.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-master/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-master/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-master/dist/css/AdminLTE.min.css') }}">
    <!-- Data Table Style-->
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.jqueryui.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.8/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">

    <!-- Printing Setting -->
    <link media="screen" href="{{ asset('css/printing.css') }}" />

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
    page. However, you can choose any other skin. Make sure you
    apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('AdminLTE-master/dist/css/skins/skin-blue.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
-->
<style>

    @font-face {
        font-family: 'THSarabunNew';
        font-style: normal;
        font-weight: normal;
        src: url('{{ asset('fonts/THSarabunNew.ttf') }}') format('truetype');
    }
    @font-face {
        font-family: 'code128';
        font-style: normal;
        font-weight: normal;
        src: url('{{ asset('fonts/code128.ttf') }}') format('truetype');
    }
    .all-font-size{
        font-size: 18px;
    }
    .barcode-output{
        font-family: 'code128';
        font-size: 36px;
    }
</style>

<body class="hold-transition skin-blue sidebar-mini">

    <!-- main contain body -->


    @include('frontend.mainbody')





    </body>
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('AdminLTE-master/bower_components/jquery/dist/jquery.min.js') }}"></script>


    <!-- Jquery Print Script -->
    <script src="{{ asset('js/jquery.printPage.js')}}"></script>
    <script src="{{ asset('js/jquery.PrintArea.js')}}"></script>
    <script src="{{ asset('js/printThis.js')}}"></script>

    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('AdminLTE-master/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('AdminLTE-master/dist/js/adminlte.min.js') }}"></script>

    <!-- JS Barcode Code 128 -->
    <script src="{{ asset('js/JsBarcode.code128.min.js')}}"></script>

    <!-- Other script -->
    <script src="{{ asset('js/general.js') }}"></script>
    <script src="{{ asset('js/decodeitem.js') }}"></script>
    <script src="{{ asset('js/wipbarcode.js')}}"></script>
    <script src="{{ asset('js/excelexportjs.js')}}"></script>
    <script src="{{ asset('js/jquery.tabletocsv.js')}}"></script>
    <script src="{{ asset('js/tableHTMLExport.js')}}"></script>
    <script src="{{ asset('js/jquery.TableCSVExport.js')}}"></script>
    <script src="{{ asset('js/condition.js')}}"></script>
    </script><script src="{{ asset('js/numeral.min.js')}}"></script>

    <!-- Data Table -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <!--<script src="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
        Both of these plugins are recommended to enhance the
        user experience. -->

    </html>

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Web Setting
    |--------------------------------------------------------------------------
    |
    | Using for general setting
    |
    |
    |--------------------------------------------------------------------------
    */


    /*  Title of website */
    'webtitle' => 'Gypman Tech',
    'weblogin' => 'Gypman Tech Login',

    /*  Website's version */
    'webversion' => '1.0.1',

    /*Compay name */
    'companyname' => 'GYPMAN TECH COMPANY LIMITED.',

];

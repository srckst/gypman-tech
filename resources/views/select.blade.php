@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <h2 class="header-select"><b>เลือกประเภทของข้อมูลใบจอง</b></h2>
                <p class="text-danger">เรียนแจ้งผู้ใช้ระบบทุกท่าน : เพื่อการทำงานที่สมบูรณ์ กรุณาใช้ google chrome ในการใช้ระบบ</p>
            </br>
            <div class="container-fluid" style="width:90%;">
                <div class="row">
                    <div class="col-lg-4 col-xs-4 text-white">
                        <!-- small box -->
                        <a data-target="#so_search" data-toggle="modal">
                            <div class="small-box bg-aqua card-shadow">
                                <div class="inner">
                                    <br>
                                    <h3 class="text-center text-so-white" style="font-size:1.4vw;">แปรรหัสออกของในประเทศ</h3>
                                    <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-dolly" style='font-size:70px;'></i>
                                </div>
                                <a data-target="#so_search" data-toggle="modal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-xs-4 text-white">
                        <!-- small box -->
                        <a data-target="#so_search" data-toggle="modal">
                            <div class="small-box bg-yellow card-shadow">
                                <div class="inner">
                                    <br>
                                    <h3 class="text-center text-so-white" style="font-size:1.4vw;">แปรรหัสโอนย้ายในประเทศ</h3>
                                    <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-truck" style='font-size:60px;'></i>
                                </div>
                                <a data-target="#so_search" data-toggle="modal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </a>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-4">
                        <!-- small box -->
                        <a data-target="#se_search" data-toggle="modal">
                            <div class="small-box bg-green card-shadow">
                                <div class="inner">
                                    <br>
                                    <h3 class="text-center text-so-white" style="font-size:1.4vw;">แปรรหัสออกของนอกประเทศ</h3>
                                    <p class="text-center text-so-white">Lorem ipsum dolor sit amet, consectetur </p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-globe-asia" style='font-size:70px;'></i>
                                </div>
                                <a data-target="#se_search" data-toggle="modal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="text-left">
                <a href="{{ route('main') }}" class="btn btn-warning"  name="button"><em class="text-white fa fa-th"><b>  กลับไปยังเมนูหลัก</b></em></a>
            </div>
        </div>

    </div>
</div>
<!-- ./col -->

<div class="modal fade" id="so_search">
    <div class="modal-dialog modal-lg" style="width:80%; height:100%;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ค้นหาใบจองภายในประเทศ</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="text-danger">สามารถค้นหาด้วยเลขที่ใบจอง หรือชื่อลูกค้าได้ในช่องค้นหา</p>
            </div>

            <!-- Modal body -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered display" id="sotable">
                        <thead>
                            <tr class="text-table-so">
                                <th class="text-center">#</th>
                                <th class="hidden-xs text-center">เลขที่ใบจอง</th>
                                <th class="hidden-xs text-center">ชื่อลูกค้า</th>
                                <th class="text-center"><em class="fa fa-cog"></em></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($so as $so)
                                <tr>
                                    <td class="text-center">{{ $countso++ }}</td>
                                    <td class="hidden-xs text-center"><a href="{{ route('cutstocks',$so->DI_KEY) }}">{{ $so->DI_REF }}</a></td>
                                    <td class="hidden-xs text-center">{{ $so->AR_NAME }}</td>
                                    <td align="center">
                                        <a href="{{ route('cutstocks',$so->DI_KEY) }}" class="btn btn-default btn-sm"><em class="fa fa-file-text" style="font-size:18px;" data-toggle="tooltip" data-placement="left" title="ดูข้อมูล"></em></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="se_search">
    <div class="modal-dialog modal-lg" style="width:80%; height:100%;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ค้นหาใบจองต่างประเทศ</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="text-danger">สามารถค้นหาด้วยเลขที่ใบจอง หรือชื่อลูกค้าได้ในช่องค้นหา</p>
            </div>

            <!-- Modal body -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered display" id="setable">
                        <thead>
                            <tr class="text-table-so">
                                <th class="text-center">#</th>
                                <th class="hidden-xs text-center">เลขที่ใบจอง</th>
                                <th class="hidden-xs text-center">ชื่อลูกค้า</th>
                                <th class="text-center"><em class="fa fa-cog"></em></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($se as $se)
                                <tr>
                                    <td class="text-center">{{ $countse++ }}</td>
                                    <td class="hidden-xs text-center"><a href="{{ route('cutstocks',$se->DI_KEY) }}">{{ $se->DI_REF }}</a></td>
                                    <td class="hidden-xs text-center">{{ $se->AR_NAME }}</td>
                                    <td align="center">
                                        <a href="{{ route('cutstocks',$se->DI_KEY) }}" class="btn btn-default btn-sm"><em class="fa fa-file-text" style="font-size:18px;" data-toggle="tooltip" data-placement="left" title="ดูข้อมูล"></em></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

@endsection

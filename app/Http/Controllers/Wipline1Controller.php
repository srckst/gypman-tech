<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Wipline1;

class Wipline1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $n =  1;
        $n2 = 1;
        $n3 = 1;
        $padnum = 1;
        $padnum2 = 1;
        $padnum3 = 1;
        $wip1 = Wipline1::all();
        $view = view('wip_line1a',compact('wip1','n','n2','n3','padnum','padnum2','padnum3'));
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = new Wipline1;

        $insert->wl1_barcode = $request->input('wl1_barcode');
        $insert->wl1_brand = $request->input('wl1_brand');
        $insert->wl1_amount = $request->input('wl1_amount');

        $insert->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Wipline1::find($id);
        $delete->delete();
        return $delete;
    }
}

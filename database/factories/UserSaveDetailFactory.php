<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(USERSAVEDETAIL::class, function (Faker $faker) {
    return [
        'id' => $faker->unique()->id,
        'se_id' => $faker->se_id,
        'se_name' => $faker->se_name,
        'se_created_at' => $faker->se_created_at,
    ];
});

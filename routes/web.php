<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/' , 'HomeController@selectfunction')->name('main');
Route::get('showname/{id}','IndexController@showname');
Route::get('/select', 'HomeController@select')->name('select');
Route::get('/home', 'HomeController@selectfunction')->name('home');
Route::get('/seshowmain', 'HomeController@seshowmain')->name('seshowmain');
Route::get('/sedetail_action', 'HomeController@sedetail_search')->name('sedetail.action');
Route::get('resetpass', 'HomeController@reset_password')->name('reset_password');

Route::get('/homese', 'HomeController@home_se')->name('home_se');
Route::get('/datatablese', 'HomeController@datatable_se')->name('datatable_se');
Route::get('/ajaxse', 'HomeController@ajax_se')->name('ajax_se');
Route::post('/addse', 'HomeController@addse')->name('addse');

Route::get('/homeso', 'HomeController@home_so')->name('home_so');
Route::get('/datatableso', 'HomeController@datatable_so')->name('datatable_so');
Route::get('/ajaxso', 'HomeController@ajax_so')->name('ajax_so');

Route::get('/viewitem', 'HomeController@viewitem')->name('viewitem');
Route::get('/viewitem', 'HomeController@viewitemact')->name('viewitemact');
Route::get('/view_item_so/{id}', 'HomeController@getitemso')->name('getitemso');
Route::get('/view_item_se/{id}', 'HomeController@getitemse')->name('getitemse');
Route::get('/resultitem', 'HomeController@resultitem');
Route::get('resultcheck', 'HomeController@resu');
Route::get('/testso/{so_oder_number}', 'HomeController@testso');
Route::get('/createso', 'HomeController@create_so')->name('createso');
Route::get('/createse', 'HomeController@create_se')->name('createse');
Route::get('/testwip', 'HomeController@wip_test');
Route::get('/scanfg', 'HomeController@fetch_fg')->name('scanfg');
Route::get('/decodeitem', 'HomeController@viewitemdecode')->name('decodeitem');
Route::post('/fetchitemcode', 'HomeController@itemdecode')->name('fetchitemcode');
Route::get('/selectline', 'HomeController@selectwipline')->name('selectwipline');
Route::delete('/deleteline1wip/{id}', 'HomeController@deletewipline1')->name('deletewipline1');
Route::delete('/deletecodefg/{id}', 'HomeController@deletefg')->name('deletecodefg');
Route::get('/gencode', 'BarcodeGenController@genwipline1a')->name('gencode');
Route::get('/printwipline1a/{id}', 'HomeController@printwipl1a')->name('printwipl1a');
Route::get('/bplusdb', 'HomeController@connectdbplus')->name('bplusdb');
Route::get('/order', 'HomeController@viewoderall')->name('viewoderall');
Route::get('/cutstocks/{DI_KEY}', 'HomeController@cutstocks')->name('cutstocks');
Route::get('/transfer', 'HomeController@transfer')->name('transfer');
Route::post('/transfer_input', 'HomeController@transfer_input')->name('transfer_input');

Route::get('/usersetting', 'UserController@index')->name('usersetting');
Route::get('/adduser', 'UserController@adduser')->name('adduser');
Route::get('/adjustuser', 'UserController@adjustuser')->name('adjustuser');

Route::resource('create_so', 'CreateSoController');
Route::resource('scanwip','ScanbarcodeController');
Route::resource('tagfg', 'FgbarcodeController');
Route::resource('wipline1a', 'Wipline1Controller');
Route::resource('wipline2', 'Wipline2Controller');

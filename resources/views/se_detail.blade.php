@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="justify-content-center">
                    <div class="card" style="box-shadow:none; transform: none;">
                        <div class="card-header"><h4><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า - ต่างประเทศ</b></h4>
                            <button data-target="#so_search" id="myBtn" data-toggle="modal" class="btn btn-info fa fa-search" type="button">  กรอกรหัสใบจองสินค้าต่างประเทศ</button>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-list">
                                <thead>
                                    <tr>
                                        <th class="text-center"><em class="fa fa-cog"></em></th>
                                        <th class="hidden-xs text-center">ID</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--
                                    <tr>
                                    <td align="center">
                                    <a class="btn btn-outline-secondary"><em class="fa fa-pencil"></em></a>
                                    <a class="btn btn-outline-danger"><em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                            </tr>
                        -->
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button  class="btn btn-success" type="button" name="button"><em class="fa fa-floppy-o">  บันทึกข้อมูล</em></button>
                    <button  class="btn btn-info" type="button" name="button"><i class="fa fa-file-excel-o">  รายงานไฟลฺ์ Excel</i></button>
                    <button class="btn btn-warning text-right" onclick="goBack()"><em class="text-white fa fa-long-arrow-left"><b>  กลับไปก่อนหน้านี้</b></em></button>
                </div>
            </div>
        </div>

    </div>
</div>



</div>
</div>

<script>


    function goBack() {
        window.location.href = "{{ route('select') }}";
    }

    $(document).ready(function(){
        $("#myBtn").click(function(){
            $("#myModal").modal();
        });
    });


</script>
@endsection

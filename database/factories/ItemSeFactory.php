<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\ItemSe::class, function (Faker $faker) {
    return [
        'id' => $faker->unique()->id,
        'oder_number' => $faker->oder_number,
        'item_no' => $faker->item_no,
        'item_list' => $faker->item_list,
        'item_amount' => $faker->item_amount,
        'item_unit_of_measure' => $faker->item_unit_of_measure,
    ];
});

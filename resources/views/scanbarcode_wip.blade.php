@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="card-header bg-white"><h2><b>ระบบสแกนบาร์โค้ด</b></h2>
                    </div>
                </div>
            </br>
            <div class="container-fluid">
                <p><b>รายการออกของ</b></p>
                <div class="row">
                    <div class="col-md-6">
                        <form class="form-inline md-form form-sm mt-0">
                            <input id="myInput" onkeyup="myFunction()" type="text" class="form-control" placeholder="รหัสสินค้า">
                            <button class="btn btn-primary btn-md" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form id="insertwip" class="form-inline md-form form-sm mt-0 text-right" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="text-right">
                            <input id="wip_barcode" name="wip_barcode" type="text" class="form-control text-center" style="width:40%;" placeholder="สแกนบาร์โค้ดตรงนี้"/>
                            <input type="hidden" name="action" id="action" />
                            <input type="hidden" name="hidden_id" id="hidden_id" />
                            <button type="submit" class="btn btn-dark btn-md" name="submit_wipcode">
                                <i class="fa fa-barcode"></i>
                            </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="myTable" class="table table-hover bg-white text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th style="width:15%;">รหัสบาร์โค้ด</th>
                                <th style="width:15%;">รหัสสินค้า</th>
                                <th>ประเภทสินค้า</th>
                                <th>วันที่</th>
                                <th>ตั้งเลขที่</th>
                                <th>จำนวน</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($wip as $wips)
                                <tr>
                                    <td></td>
                                    <td>{{ $wips->wip_barcode }}</td>
                                    <td class="codewip2">{{ $wips->wip_barcode }}</td>
                                    <td class="codewip3">{{ $wips->wip_barcode }}</td>
                                    <td class="codewip4">{{ $wips->wip_barcode }}</td>
                                    <td class="codewip5">{{ $wips->wip_barcode }}</td>
                                    <td class="codewip6">{{ $wips->wip_barcode }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>รหัสบาร์โค้ด</th>
                                <th>รหัสสินค้า</th>
                                <th>ประเภทสินค้า</th>
                                <th>วันที่</th>
                                <th>ตั้งเลขที่</th>
                                <th>จำนวน</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="text-right">
                <a href="{{ route('main') }}" class="text-white btn btn-success"><b>&nbsp;&nbsp;&nbsp;รายงานไฟล์ CRV</b></a>
                <a href="{{ route('main') }}" class="text-white btn btn-warning fa fa-home"><b>&nbsp;&nbsp;&nbsp;ไปยักลับไปยังเมนูหลัก</b></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    // Count row table
    var table = document.getElementsByTagName('table')[0],
    rows = table.getElementsByTagName('tr'),
    text = 'textContent' in document ? 'textContent' : 'innerText';

    for (var i = 1, len = rows.length; i < len-1; i++){
        rows[i].children[0][text] = i  + rows[i].children[0][text];
    }

</script>

@endsection

@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="card-header bg-white"><h4><b>สร้างใบส่งของ</b></h4>
                        <p class="text-danger">สารมารถรองรับการสแกนด้วยบาร์โค้ด โดยกดที่ปุ่มค้นหาด้ค้นหาด้วยการสแกนบาร์โค้ด</p>
                    </br>
                    <button data-target="#search_key_so" data-toggle="modal" class="btn btn-info" type="button" name="button"><em class="text-white fa fa-keyboard-o">  ค้นหาด้วยการพิมรหัส</em></button>
                    <button data-target="#search_barcode_so"  data-toggle="modal" class="btn btn-primary" type="button" name="button"><em class="text-white fa fa-barcode">  ค้นหาด้วยการสแกนบาร์โค้ด</em></button>
                </div>
            </div>
        </br>
        <h5><b>รายการสินค้าที่นำส่ง</b></h5>
    </br>
    <form class="form-inline md-form form-sm mt-0">
        <input id="myInput" onkeyup="myFunction()" type="text" class="form-control" placeholder="เลขที่ล็อต">
        <button class="btn btn-info btn-md" type="button">
            <i class="fa fa-search"></i>
        </button>
    </form>

    <table class="table table-hover" id="myTable">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">รหัสสินค้า</th>
                <th class="text-center">เลขที่ล็อต</th>
                <th class="text-center">รายการ</th>
                <th class="text-center">จำนวน</th>
                <th class="text-center">หน่วยนับ</th>
                <th class="text-center"><em class="text-dark fa fa-cog"></em></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>

    <div class="container-fluid">
        <hr>
        <div class="text-right">
            <a href="#" class="btn btn-success" name="button"><em class="text-white fa fa-save"><b>  บันทึกข้อมูล</b></em></a>
            <a href="#" class="btn btn-danger"  name="button"><em class="text-white fa fa-close"><b>  ยกเลิกเอกสาร</b></em></a>
            <a href="{{ route('select') }}" class="btn btn-warning"  name="button"><em class="text-white fa fa-long-arrow-left"><b>  กลับไปหน้าค้นหาข้อมูล</b></em></a>
        </div>
    </div>
</div>
</div>
</div>

<div class="modal fade" id="search_key_so">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ค้นหาด้วยการพิมรหัส</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="panel-body">
            </br>
            <input id="iconified" type="text" maxlength="24" class="form-control empty" placeholder="&#xf11c;   พิมรหัสตรงนี้...">
        </br>
        <input type="submit" class="btn btn-success btn-md fa" style="font-size:15px;" name="create_so" value="&#xf0fe;&nbsp;&nbsp;&nbsp;เพิ่มข้อมูล">

    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
    </div>
</div>
</div>
</div>

<div class="modal fade" id="search_barcode_so">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">ค้นหาด้วยบาร์โค้ด</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="panel-body">
            </br>
            <input id="iconified" type="text" maxlength="24" class="form-control empty" placeholder="&#xf02a;   สแกนบาร์โค้ดจากใบจอง">
        </br>
        <input type="submit" class="btn btn-success btn-md fa" style="font-size:15px;" name="create_so" value="&#xf0fe;&nbsp;&nbsp;&nbsp;เพิ่มข้อมูล">
    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
    </div>
</div>
</div>
</div>
<script>

    $('table tbody tr').each(function(idx){
        $(this).children().first().html(idx + 1);
    });

    var table = document.getElementsByTagName('table')[0],
    rows = table.getElementsByTagName('tr'),
    text = 'textContent' in document ? 'textContent' : 'innerText';

    for (var i = 1, len = rows.length; i < len-1; i++){
        rows[i].children[0][text] = i  + rows[i].children[0][text];
    }
</script>

@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FgTransfer extends Model
{
    protected $table = 'fg_transfer';

    protected $fillable = ['fg_code_transfer'];
}

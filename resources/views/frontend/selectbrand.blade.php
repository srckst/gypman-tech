    <select id="wl1_brand" name="wl1_brand" class="margin-select selectpicker show-tick form-control" aria-required="true" data-size="5" data-dropup-auto="false" data-live-search="true" data-style="btn-info btn-sm text-dark" data-width="fit" required>
        <option value="">เลือกแบรนด์</option>
        <option data-tokens="00" value="00">00 - แผ่นเปลือย</option>
        <option data-tokens="01" value="01">01 - VIP (มอก.)</option>
        <option data-tokens="04" value="04">04 - GM</option>
        <option data-tokens="05" value="05">05 - SHOGUN</option>
        <option data-tokens="06" value="06">06 - เพชร5ดาว</option>
        <option data-tokens="09" value="09">09 - เกรดC</option>
        <option data-tokens="10" value="10">10 - ต้นไม้</option>
        <option data-tokens="11" value="11">11 - SCL</option>
        <option data-tokens="12" value="12">12 - RPG</option>
        <option data-tokens="13" value="13">13 - SUPER</option>
        <option data-tokens="14" value="14">14 - NOLOGO</option>
        <option data-tokens="15" value="15">15 - ตราเพชร</option>
        <option data-tokens="16" value="16">16 - ST</option>
        <option data-tokens="17" value="17">17 - 3G</option>
        <option data-tokens="18" value="18">18 - Maxum</option>
        <option data-tokens="19" value="19">19 - City bord</option>
        <option data-tokens="20" value="20">20 - สีธรรมดา</option>
        <option data-tokens="21" value="21">21 - ขาวผ่อง</option>
        <option data-tokens="22" value="22">22 - อดามาส</option>
        <option data-tokens="23" value="23">23 - นาเดีย</option>
        <option data-tokens="24" value="24">24 - โชกุน</option>
        <option data-tokens="25" value="25">25 - บิ๊ก-บอย</option>
        <option data-tokens="26" value="26">26 - ยิปแม๊ก</option>
        <option data-tokens="27" value="27">27 - TRUSUS</option>
        <option data-tokens="28" value="28">28 - OPSKY</option>
        <option data-tokens="29" value="29">29 - DD bord</option>
        <option data-tokens="30" value="30">30 - CC</option>
        <option data-tokens="31" value="31">31 - GM กัมพูชา</option>
        <option data-tokens="32" value="32">32 - TOA</option>
        <option data-tokens="33" value="33">33 - HOFF GM</option>
        <option data-tokens="34" value="34">34 - DIC</option>
        <option data-tokens="94" value="94">94 - รอติดFOIL</option>
        <option data-tokens="9W" value="9W">9W - รอติดWAX</option>
        <option data-tokens="99" value="99">99 - แผ่นรอคัด</option>
        <option data-tokens="44" value="44">44 - GM เบาๆ</option>
        <option data-tokens="9H" value="9H">9H - รอเคลือบ WAX เจาะรูคู่</option>
        <option data-tokens="9O" value="9O">9O - รอเคลือบ WAX เจาะรูเดี่ยว</option>
    </select>

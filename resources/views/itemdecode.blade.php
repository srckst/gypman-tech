@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">

            </br>
            <div class="container-fluid">
                <p><b>รายการสินค้า</b></p>
                <form class="form-inline md-form form-sm mt-0">
                    <input id="myInput" onkeyup="myFunction()" type="text" class="form-control" placeholder="รหัสสินค้า">
                    <button class="btn btn-primary btn-md" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
                <div class="col-md-6">
                    <form id="itemdecodeform" class="form-inline md-form form-sm mt-0 text-right" enctype="multipart/form-data" method="post">
                        @csrf
                        <input id="item_code" name="item_code" type="text" class="form-control text-center" style="width:40%;" placeholder="ใส่รหัสสินค้า"/>
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <button type="submit" class="btn btn-dark btn-md" name="submit_code">
                            <i class="fa fa-barcode"></i>
                        </button>
                    </form>
                </div>

                <table id="myTable" class="table table-hover bg-white text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>รหัสสินค้า</th>
                            <th>ชนิดสินค้า</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($itemcode as $itemcodes)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $itemcodes->item_code }}</td>
                                <td class="idc1">{{ $itemcodes->item_code }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>รหัสสินค้า</th>
                            <th>ชนิดสินค้า</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-table-so">
                                    <th class="hidden-xs text-center">#</th>
                                    <th class="text-center">Username</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center"><em class="fa fa-cog"></em></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $users)
                                    <tr>
                                        <td class="hidden-xs text-center">{{ $count++ }}</td>
                                        <td class="text-center">{{ $users->username }}</td>
                                        <td class="text-center">{{ $users->name }}</td>
                                        <td class="text-center">{{ $users->email }}</td>
                                        <td align="center">
                                            <a href="" class="btn btn-default btn-sm"><em class="fa fa-file-text" style="font-size:18px;" data-toggle="tooltip" data-placement="left" title="ดูข้อมูล"></em></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
